﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowWindow : MonoBehaviour
{

    public Transform target;
    public Transform target2;
    public Vector3 zOffset = new Vector3(0, 0, -10);
    public Rect window = new Rect(0, 0, 1, 1);


    void Update()
    {
        // get the position of the target
        Vector3 targetPos = target.position; // world coords
 
        // convert to viewport coordinates
        Vector2 targetViewPos =
            Camera.main.WorldToViewportPoint(targetPos);

        // clamp this position to 
        // the closest point inside the window
        Vector2 goalViewPos = window.Clamp(targetViewPos);

        // convert back to world coordinates
        Vector3 goalPos =
            Camera.main.ViewportToWorldPoint(goalViewPos);

        // convert both points into the local coordinate
        // system of the camera
        targetPos = transform.InverseTransformPoint(targetPos);
        goalPos = transform.InverseTransformPoint(goalPos);

        // compute the necessary camera movement in the xy plane
        // for the target to appear at the goal
        Vector3 move = targetPos - goalPos;
        move.z = 0;

        transform.Translate(move);

        //target2
        // get the position of the target
        Vector3 target2Pos = target2.position; // world coords

        // convert to viewport coordinates
        Vector2 target2ViewPos =
            Camera.main.WorldToViewportPoint(target2Pos);

        // clamp this position to 
        // the closest point inside the window
        Vector2 goal2ViewPos = window.Clamp(target2ViewPos);

        // convert back to world coordinates
        Vector3 goal2Pos =
            Camera.main.ViewportToWorldPoint(goal2ViewPos);

        // convert both points into the local coordinate
        // system of the camera
        target2Pos = transform.InverseTransformPoint(target2Pos);
        goal2Pos = transform.InverseTransformPoint(goal2Pos);

        // compute the necessary camera movement in the xy plane
        // for the target to appear at the goal
        Vector3 move2 = target2Pos - goal2Pos;
        move2.z = 0;

        transform.Translate(move2);
    }


    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        for (int i = 0; i < 4; i++)
        {
            Vector3 f =
             Camera.main.ViewportToWorldPoint(window.Corner(i))
              - zOffset;
            Vector3 t =
             Camera.main.ViewportToWorldPoint(window.Corner(i + 1))
              - zOffset;

            Gizmos.DrawLine(f, t);
        }

    }
}