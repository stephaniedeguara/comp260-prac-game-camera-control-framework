﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSize : MonoBehaviour {
    public Transform target;
    public Transform target2;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float distance = Vector3.Distance(target.transform.position, target2.transform.position);
        Debug.Log(distance);
        Camera.main.orthographicSize = distance + 1;
    }
}
